"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'my-app',
        template: "\n      <div class=\"container\">\n          <div class=\"row mb-3 mt-3\">\n              <ul class=\"nav col-7\">\n                  <li class=\"nav-item\">\n                      <a class=\"nav-link\" routerLink=\"/\" routerLinkActive=\"active\">\u0417\u0430\u043C\u0435\u0442\u043A\u0438</a>\n                  </li>\n                  <li class=\"nav-item\">\n                      <a class=\"nav-link\" routerLink=\"/create\" routerLinkActive=\"active\" href=\"#\">\u0421\u043E\u0437\u0434\u0430\u0442\u044C</a>\n                  </li>\n              </ul>\n              <div class=\"col-5 text-right\"><a href=\"/logout\" class=\"btn btn-link\">\u0412\u044B\u0439\u0442\u0438</a></div>\n          </div>\n          <router-outlet></router-outlet>\n      </div>\n  ",
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map