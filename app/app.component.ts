import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `
      <div class="container">
          <div class="row mb-3 mt-3">
              <ul class="nav col-7">
                  <li class="nav-item">
                      <a class="nav-link" routerLink="/" routerLinkActive="active">Заметки</a>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" routerLink="/create" routerLinkActive="active" href="#">Создать</a>
                  </li>
              </ul>
              <div class="col-5 text-right"><a href="/logout" class="btn btn-link">Выйти</a></div>
          </div>
          <router-outlet></router-outlet>
      </div>
  `,
})
export class AppComponent  { }
