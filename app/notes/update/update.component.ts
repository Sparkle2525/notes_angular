import {Component, OnInit} from '@angular/core';
import {Category, Note} from "../shared/notes";
import {NoteService} from "../shared/notes.service";
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';
import {Subscription} from "rxjs/Subscription";

@Component({
    selector: 'app-update',
    templateUrl: './update.component.html'
})
export class UpdateComponent implements OnInit {
    public options: Object = {
        placeholderText: 'Edit Your Content Here!',
        charCounterCount: false,
        height: 300
    };
    categories: Category[];
    selectedCatObj: Category;
    note: Note;
    private subscription: Subscription;
    constructor(private noteService: NoteService, private route: ActivatedRoute,
                private location: Location) {}
    ngOnInit(): void {
        this.subscription = this.route.paramMap
            .switchMap((params: ParamMap) => this.noteService.getNote(+params.get('id'))).subscribe(note => {
                this.noteService.getCategories().then(cats => {
                    this.note = note;

                    this.categories = cats;
                    if (cats.length) {
                        for (let cat of cats) {
                            if (cat.id === note.category.id ) {
                                this.selectedCatObj = cat;
                            }
                        }
                    }
                });
            });
    }
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    goBack(): void {
        this.location.back();
    }
    update(): void {
        this.note.category = this.selectedCatObj;
        this.noteService.updateNote(this.note).then(() => {
            this.goBack();
        });
    }
}
