
export class Note {
    id: number;
    title: string;
    content: string;
    creation: Date;
    category: Category;
    favourite: boolean;
    published: boolean;
    linkVisible: boolean = false;
}

export class Category {
    id: number;
    name: string;
}

