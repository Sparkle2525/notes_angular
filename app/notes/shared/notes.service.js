"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var http_2 = require("@angular/http");
var NoteService = (function () {
    function NoteService(http) {
        this.http = http;
        this.notesUrl = 'http://localhost:8000/api/notes';
        this.addToFavouritesUrl = 'http://localhost:8000/api/add_to_favourites';
        this.publishUrl = 'http://localhost:8000/api/publish';
        this.categoriesUrl = 'http://localhost:8000/api/categories/';
        this.headers = new http_2.Headers({ 'Content-Type': 'application/json' });
    }
    NoteService.prototype.getNotes = function () {
        return this.http.get(this.notesUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    NoteService.prototype.getCategories = function () {
        return this.http.get(this.categoriesUrl)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    NoteService.prototype.getNote = function (id) {
        var url = this.notesUrl + "/" + id;
        return this.http.get(url)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    NoteService.prototype.createNote = function (note) {
        return this.http
            .post(this.notesUrl, JSON.stringify(note), { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    NoteService.prototype.updateNote = function (note) {
        // note.date.setHours(note.date.getHours() - note.date.getTimezoneOffset() / 60);
        var url = this.notesUrl + "/" + note.id;
        return this.http
            .put(url, JSON.stringify(note), { headers: this.headers })
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    NoteService.prototype.deleteNote = function (id) {
        var url = this.notesUrl + "/" + id;
        return this.http.delete(url, { headers: this.headers })
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    NoteService.prototype.addToFavourites = function (id) {
        return this.http.post(this.addToFavouritesUrl, JSON.stringify({ id: id }))
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    NoteService.prototype.publish = function (id) {
        return this.http.post(this.publishUrl, JSON.stringify({ id: id }))
            .toPromise()
            .then(function () { return null; })
            .catch(this.handleError);
    };
    NoteService.prototype.handleError = function (error) {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    };
    return NoteService;
}());
NoteService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], NoteService);
exports.NoteService = NoteService;
//# sourceMappingURL=notes.service.js.map