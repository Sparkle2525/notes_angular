"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var notes_component_1 = require("./notes.component");
var create_component_1 = require("./create/create.component");
var update_component_1 = require("./update/update.component");
var routes = [
    { path: '', redirectTo: 'notes', pathMatch: 'full' },
    { path: 'notes', component: notes_component_1.NotesComponent },
    { path: 'notes/:id', component: update_component_1.UpdateComponent },
    { path: 'create', component: create_component_1.CreateComponent }
];
var NotesRoutingModule = (function () {
    function NotesRoutingModule() {
    }
    return NotesRoutingModule;
}());
NotesRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(routes)],
        exports: [router_1.RouterModule]
    })
], NotesRoutingModule);
exports.NotesRoutingModule = NotesRoutingModule;
//# sourceMappingURL=notes-routing.module.js.map